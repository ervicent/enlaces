# enlaces

Enlaces de materiales

# MISCELÁNEA

* https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-es.md?s=09#javascript (Libros de todos los módulos)
* https://dogramcode.com/bloglibros/index (Libros en pdf)
* http://informatica.gonzalonazareno.org/plataforma/course/index.php?categoryid=5 (Moodle con cursos guest)
* https://aulavirtual.iesthosicodina.cat/moodle/course/view.php?id=43  (Moodle con cursos guest)
* https://sites.google.com/site/carlospesrivas/recursos/informatica-ciclos-formativos-de-grado-superior (Listadod de cursos de SMX, DAW y ASIX)
* https://aulavirtual35.educa.madrid.org/aulas/course/view.php?id=171 (Moodle de grado medio)

# SMX

## M01 - Montaje y mantenimiento de equipos informáticos
* https://mestreacasa.gva.es/web/guest/informaticaicomunicacions (Ejercicios binario)
* http://www.carm.es/edu/pub/04_2015/3_2_2_contenido.html (Particionado, congelado y clonación)
* https://www.ajpdsoft.com/modules.php?name=News&file=article&sid=380 (Fstab y mount)
* https://www.examcompass.com/ (Éxamenes tipo test montaje)
* https://tecadmin.net/install-virtualbox-on-ubuntu-18-04/ (Instalar vbox en ubuntu)

## M02 - Sistemas operativos
* https://books.google.es/books?id=aKxFDwAAQBAJ&lpg=PA13&dq=practica%20cuotas%20de%20disco%20linux&hl=es&pg=PP1#v=onepage&q&f=false (Cuadernillo práctico de Linux)
* https://fortinux.gitbooks.io/humble_tips/content/ (Administración Linux)
* https://www.softzone.es/linux/tutoriales/instalar-configurar-wine/ (Instalación de wine sobre Ubuntu)
* https://www.profesionalreview.com/2021/10/20/como-instalar-windows-11-en-virtualbox/ (Instalar Windows 11 en VirtualBox)
* https://overthewire.org/wargames/bandit/bandit0.html (Juego por niveles para practicar comandos por ssh)

## M03 - Ofimática
* http://elprofe.cat/	(LibreOffice, photoshop, fotografía, gimp)
* https://joapuiib.github.io/itb/BATX-TIC1/ (LibreOffice Calc)

## M04 - Sistemas operativos en red
* https://www.ticarte.com/contenido/administracion-de-sistemas-operativos (Administración Windows Server y Linux)
* http://somebooks.es/ (Administración Linux, Windows Server y virtualización)
* https://www.profesionalreview.com/2018/12/23/enrutamiento-windows-server-2016/ (Enrutamiento Windows Server)
* https://sergi-coll.gitbook.io/sox (UF's M04)

## M05 - Redes
* https://www.redeszone.net/marcas/tp-link/configuracion-ap-tp-link-tl-wa701nd-tl-wa901nd/amp/ (Configurar una AP con D-link)
* https://oscarmaestre.github.io/apuntes_redes/index.html (Temario completo)

## M06 - Seguridad informática
* https://sites.google.com/site/seguridadinformaticaisidro/sobre-esta-pagina-1 (Seguridad informática)
* https://fp.josedomingo.org/seguridadgm/ (Moodle con actividades)

## M07 - Servicios en red
* https://tonynformatica.wordpress.com/category/sri/ (Servicios en RED: FTP, exchange y DNS)
* https://tonynformatica.wordpress.com/2019/02/07/instalacion-y-configuracion-de-servidor-de-correo-postfix-y-dovecot-postfixadmin-squirrelmail-y-mailman/ (servidor de correo Postfix y Dovecot (PostfixAdmin, Squirrelmail))
* https://dit.gonzalonazareno.org/moodle/course/view.php?id=21 (Gonzalo Nazareno -> Servicios en red)
* https://practiquessisxarxa.wordpress.com/ (Apache, wordpress, nextcloud)
* https://juaky.github.io/serveis-en-xarxa/ (Serveis en xarxa)
* https://serviciosgs.readthedocs.io/es/latest/introduccion/index.html (Serveis de xarxa)
* https://www.digitalocean.com/community/tutorials/how-to-set-up-vsftpd-for-anonymous-downloads-on-ubuntu-16-04#step-5-%E2%80%94-testing-anonymous-access (servdiro vsftp)
* https://www.linuxhispano.net/2011/02/17/configurar-un-servidor-de-ftp-anonimo-con-vsftpd/ (servdiro vsftp)
* https://www.solvetic.com/tutoriales/article/5811-como-instalar-y-configurar-vsftpd-ubuntu-18/ (configurar usuarios del sistema en vsftp y anonymous)

## M08 - Aplicaciones web
* https://phoenixnap.com/kb/how-to-install-xampp-on-ubuntu (Instalar xampp en ubuntu 18.04)
* https://www.dinorunn.com/how-to-create-xampp-shortcut-in-ubuntu-start-menu/ (Lanzador xampp ubuntu)
* https://oscarmaestre.github.io/lenguajes_marcas/tema2.html (HTML5)
* https://oscarmaestre.github.io/lenguajes_marcas/tema3.html (CSS)
* https://www.piensasolutions.com/wordpress (hosting gratuito wordpress)

## M09 - Disseny d’interfícies web
* https://desarrolloweb.com/articulos/instalar-ssl-letsencrypt-ubuntu-apache.html (Certificados apache)
* https://tecadmin.net/install-lets-encrypt-create-ssl-ubuntu/ (Certificados apache)

# M12 - Projecte
* https://techmake.com/blogs/tutoriales/empezando-con-arduino-4b-lcd-sensor-de-temperatura (Projectes arduino)

## M14 - Introducción a la programación
* https://es.py4e.com/lessons (Python desde 0)
* https://aprendepython.es/ (Python desde 0)
* https://lm.readthedocs.io/en/latest/unidades/u3/ (Python con lenguaje de marcas)
* https://gitlab.com/brian_python/entreno (Ejercicios python básico)
* https://github.com/AFIF-UG/Introduccion_a_Python-Curso_Online (Curso python desde 0)
* https://github.com/josedom24/curso_python3 (Curso python desde 0)
* https://codehs.com/ide (Plataforma enseñar python con ejemplos)
* https://codecombat.com/ (Plataforma gamer para aprender python)

# DAW

# M01 - Sistemas informáticos
* https://joapuiib.github.io/itb/DAM-M01/ (Moodle con todas las UF's)

## M02 - Bases de datos
* https://bbdd.codeandcoke.com/start (Bases de datos y objeto relacional)
* https://tonynformatica.wordpress.com/2018/10/09/instalacion-de-oracle-database/ (Instalación de oracle en Windows)
* https://joapuiib.github.io/itb/DAW-BBDD/ (Curso con todas las UF's)

## M03 - Programación
* https://github.com/statickidz/TemarioDAW (Temario de DAW)
* https://github.com/GHackAnonymous/ProgramacionAvanzada?files=1 (Programación avanzada JAVA)
* https://github.com/LuisJoseSanchez/aprende-java-con-ejercicios/tree/master/soluciones/09_POO (Programación avanzada JAVA)
* https://www.sitiolibre.com/curso/gallery.xml (Ejercicios de programación)
* https://github.com/manueldevjour/aprende-java-daw (Programación básica DAW)
* https://github.com/Gmuela/CursoJava/tree/master/Java (Programación avanzada con sockets y RMI)
* https://joapuiib.github.io/itb/DAM-M03/ (Curso completo de programación)
* https://elvex.ugr.es/decsai/c/#apuntes (programación en C)

## M04 - Lenguaje de marcas
* https://joapuiib.github.io/itb/ASIX-LLMQ/ (Moodle con todas las UF's)
* https://oscarmaestre.github.io/lenguajes_marcas/index.html (Apuntes de todas las UF's)
* https://oscarmaestre.github.io/lenguajes_marcas/tema5.html (Xpath y XQuery)

## M06 - Desarrollo web cliente
* https://www.arkaitzgarro.com/javascript/

## M07 - Desarrollo web servidor
* https://seicoll.github.io/DAW-M07-UF1-Apunts/ (UF's de M07)

# ASIX

## M08 - Servicios de red e internet
* https://fp.josedomingo.org/sri2122/ (Actividades DHCP, HTTP, DNS, HTTPS, Servidor de correo electrónico
Cluster de alta disponibilidad)
* https://elpuig.xeill.net/Members/vcarceler/asix-m08 (Actividades y teoria)
* https://cibersec.iescampanillas.com/archivos/3080 (Ciberseguridad)

## M09 - Implantación de aplicaciones web
* https://fp.josedomingo.org/iaw2122/ (PHP, PYTHON, JAVA y DOCKER)
* https://elpuig.xeill.net/Members/vcarceler/asix-m09 (Actividades y teoría)

## M10 - Administración de sistemas gestores de bases de datos
* https://ourcodeworld.com/articles/read/748/how-to-reset-mysql-5-7-root-password-in-ubuntu-16-04 (Resetar contraseña root mysql)
* https://tableplus.com/blog/2018/10/how-to-create-a-superuser-in-mysql.html (Crear superusuario mysql)
* https://wiki.lordwektabyte.cat/wiki/ASIX/M10/UF1 - (DCL con mysql, sqlserver y postgresql)
* https://wiki.lordwektabyte.cat/wiki/ASIX/M10/UF2 - (DCL con mysql, sqlserver y postgresql)
* https://joapuiib.github.io/itb/ASIX-SGBD/ (Moodle con todas las UF's)

# M11 - Seguridad y alta disponibilidad
* https://oscarmaestre.github.io/apuntes_sad/index.html (Apuntes con todas las UF's)
* https://oscarmaestre.github.io/apuntes_sad/index.html

# DAM

## M06 - Accès a dades
* https://gerardfp.github.io/dam/dades/persistence/ (Persistència en fitxers) 
* https://datos.codeandcoke.com/start (Acceso a datos)
* https://github.com/joseluisgs/AccesoDatos-00-2021-2022 (Acceso a datos)
* https://chuletasinformaticas.wordpress.com/2018/03/19/neodatis/ (Base datos orientada a objeto neodatis)
* https://oscarmaestre.github.io/lenguajes_marcas/tema6.html (Acceso a xml con xpath)

## M09 - Programacion de servicios y procesos
* https://oscarmaestre.github.io/servicios/index.html (Temario completo)

## M16 - Inteligencia artificial

* https://joapuiib.github.io/itb/DAM-M16/ (Curso con 33 horas hecho en lenguaje python)
* https://trello.com/b/GHyKqcqI/wordpress-cibernarium (Listado de cursos de inteligencia artificial)

# PROGRAMAME

* https://joapuiib.github.io/itb/ProgramaMe/ (Presentación en qué consiste el concurso)

# LIBROS

* https://drive.google.com/drive/folders/1-P0_a3QKgN7Vwt6PXJzhpSZrPPdPp9uB
